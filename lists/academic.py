#!/usr/bin/python
# -*- coding: utf-8 -*-

def getlist():
    liststr = """
scitation.org
acs.org
rcs.org
rcs-cdn.org
rcs.li
aps.org
iop.org
tandfonline.com
elsevier.com
sciencedirect.com
sciencemag.org
nature.com
pnas.org
wiley.com
cnki.net
cnki.com.cn
"""
    return set(liststr.splitlines(False))
